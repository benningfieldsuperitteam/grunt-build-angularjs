# grunt-build-angularjs README.md

## Install

Install the grunt task.

```sh
yarn add --dev @benningfield-group/grunt-build-angularjs
```

If you want to support old browsers that don't support ES5, a polyfill needs to be installed.

### @babel/polyfill

The Babel project provides a working polyfill.

Install with yarn.

```sh
yarn add @babel@polyfill
```

Include it before all other script tags.

```html
<script src="node_modules/@babel/polyfill/dist/polyfill.min.js"></script>
```

## Configuration

Create a Gruntfile.js and set up the gruntBuildAngularJS task.  The task takes two options.

1. angularModuleName: A string that is the name of your base AngularJS module.
   Note that there _must_ be a JS file in the root directory matching this name
   (e.g. BGI.GruntBuildAngularJSExample.js in the example below).
2. indexFile: An optional string that specifies the entry point for your
   single-page application.  This option defaults to "index.html."

```javascript
module.exports = function(grunt) {
  'use strict';

  grunt.loadNpmTasks('@benningfield-group/grunt-build-angularjs');

  grunt.initConfig({
    buildAngularJS: {
      prod: {
        options: {
          angularModuleName: 'BGI.GruntBuildAngularJSExample',
          indexFile: 'index.html'
        }
      }
    }
  });

  grunt.registerTask('default', [
    'buildAngularJS'
  ]);
};
```

In your index file, specify build comments.  Note that the final JavaScript
build tag is important.  It must follow the format `\`js/${angularModuleName}.min.js\``
(e.g. `js/BGI.GruntBuildAngularJSExample.min.js` in the example below.

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>grunt-build-angularjs-example</title>

    <!-- build:css css/lib.min.css -->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <!-- endbuild -->

    <!-- build:css css/grunt-build-angularjs.min.css -->
    <link rel="stylesheet" href="clock/clock.css">
    <!-- endbuild -->

    <!-- build:js js/polyfill.min.js -->
    <script src="node_modules/@babel/polyfill/dist/polyfill.js"></script>
    <!-- endbuild -->
  </head>

  <body ng-app="BGI.GruntBuildAngularJSExample">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          An example of the grunt-build-angularjs Grunt task.
        </div>
      </div>

      <clock></clock>
    </div>

    <!-- build:js js/lib.min.js -->
    <script src="node_modules/angular/angular.min.js"></script>
    <!-- endbuild -->

    <!-- build:js js/BGI.GruntBuildAngularJSExample.min.js -->
    <script src="BGI.GruntBuildAngularJSExample.js"></script>
    <script src="clock/clock.js"></script>
    <!-- endbuild -->
  </body>
</html>
```
### Building

Simply run ```grunt```.  You can get some additional output using the `-v`
(verbose) flag.  The resulting built project will be in a `build` folder.

### Examples

See the @benningfield-group/grunt-build-angularjs-example repository:
<https://bitbucket.org/benningfieldsuperitteam/grunt-build-angularjs-example>

Use in a real application:
<https://bitbucket.org/benningfieldsuperitteam/amc-v50/src/develop/V50/admin/exhibits/cancellations/>
