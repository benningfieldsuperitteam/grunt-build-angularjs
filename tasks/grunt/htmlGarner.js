module.exports = function() {
  'use strict';

  const glob = require('glob');

  return glob.sync('**/*.html').filter(function(file) {
    return !file.match(/^node_modules/) &&
           !file.match(/^bower_components/) &&
           !file.match(/^build/) &&
           !file.match(/^index.html/) &&
           !file.match(/^.tmp/);
  });
};

