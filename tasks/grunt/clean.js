module.exports = function(grunt, buildDir, tmpDir) {
  'use strict';

  const clean = {
    build: [buildDir],
    tmp:   [tmpDir]
  };

  return clean;
};

