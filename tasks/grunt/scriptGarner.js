module.exports = function(appName) {
  'use strict';

  const glob       = require('glob');
  const entryPoint = `${appName}.js`;

  return [entryPoint].concat(glob.sync('**/*.js').filter(function(script) {
    return !script.match(/^node_modules/) &&
           !script.match(/^bower_components/) &&
           script !== entryPoint &&
           !script.match(/^build/) &&
           !script.match(/^.tmp/) &&
           !script.match(/^grunt/i) &&
           !script.match(/Spec.js$/);
  }));
};

