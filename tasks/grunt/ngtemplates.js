module.exports = function(grunt, appName, html, tmpDir) {
  'use strict';

  const ngtemplates = {
    [appName]: {
      cwd:     './',
      src:     html,
      dest:    `${tmpDir}js/templates.js`,
      htmlmin: {
        removeComments    : true,
        collapseWhitespace: true
      },
      options: {
        usemin: `js/${appName}.min.js`
      }
    }
  };

  return ngtemplates;
};

