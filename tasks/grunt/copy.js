module.exports = function(grunt, srcIndex, buildIndex) {
  'use strict';

  const copy = {
    html: {
      src:  srcIndex,
      dest: buildIndex
    }
  };

  return copy;
};

