module.exports = function(grunt) {
  'use strict';

  const restore = {};
  const savable = ['clean', 'jshint', 'copy', 'filerev', 'babel'];

  grunt.registerTask('buildAngularJSRestore', 'Save task for grunt-build-angularjs.', function() {
    // Restore any saved tasks.
    savable.forEach(task => {
      if (restore[task])
        grunt.config(task, restore[task]);
    });
  });

  grunt.registerMultiTask('buildAngularJS', 'Build an AngularJS application.', function() {
    const options = this.options({
      indexFile: 'index.html'
    });

    if (!options.angularModuleName) {
      grunt.log.error('angularModuleName is required.');
      return false;
    }

    grunt.verbose.writeln(`Angular module name: ${options.angularModuleName}.`);

    if (!grunt.file.exists(options.indexFile)) {
      grunt.log.error(`indexFile "${options.indexFile}" does not exist.`);
      return false;
    }

    grunt.verbose.writeln(`Index file: ${options.indexFile}.`);

    // Load the grunt tasks required to build.
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-filerev');
    grunt.loadNpmTasks('grunt-babel');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-filerev');
    grunt.loadNpmTasks('grunt-angular-templates');
    grunt.loadNpmTasks('grunt-usemin');

    // Glob together scripts.
    const scripts  = require('./grunt/scriptGarner')(options.angularModuleName);
    grunt.verbose.writeln('Building the following JS files.');
    grunt.verbose.writeln(JSON.stringify(scripts, null, 2));

    // Glob together HTML.
    const html = require('./grunt/htmlGarner')();
    grunt.verbose.writeln('Building the following HTML files.');
    grunt.verbose.writeln(JSON.stringify(html, null, 2));

    const path       = require('path');
    const srcIndex   = options.indexFile;
    const buildDir   = 'build/';
    const tmpDir     = '.tmp/';
    const buildIndex = `${buildDir}${path.basename(options.indexFile)}`;

    // Save any existing tasks (e.g. the user may have a special "copy" or
    // "clean" task that needs to happen after the build).
    savable.forEach(task => {
      if (grunt.config(task))
        restore[task] = grunt.config(task);
    });

    // Now configure and run each task.
    grunt.config('clean',          require('./grunt/clean')(grunt, buildDir, tmpDir));
    grunt.config('jshint',         require('./grunt/jshint')(grunt, scripts));
    grunt.config('copy',           require('./grunt/copy')(grunt, srcIndex, buildIndex));
    grunt.config('useminPrepare',  require('./grunt/useminPrepare')(grunt, srcIndex, buildDir));
    grunt.config('ngtemplates',    require('./grunt/ngtemplates')(grunt, options.angularModuleName, html, tmpDir));
    grunt.config('filerev',        require('./grunt/filerev')(grunt, buildDir));
    grunt.config('usemin',         require('./grunt/usemin')(grunt, buildIndex));
    grunt.config('babel',          require('./grunt/babel-ng')(grunt, tmpDir));

    grunt.task.run('clean');
    grunt.task.run('jshint');
    grunt.task.run('copy');
    grunt.task.run('useminPrepare');
    grunt.task.run('ngtemplates');
    grunt.task.run('concat');
    grunt.task.run('babel');
    grunt.task.run('uglify');
    grunt.task.run('cssmin');
    grunt.task.run('filerev');
    grunt.task.run('usemin');
    grunt.task.run('clean:tmp');
    grunt.task.run('buildAngularJSRestore');
  });
};
